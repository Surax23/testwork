﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SearchApp
{
    /// <summary>
    /// Класс для поиска файлов.
    /// </summary>
    class FindFiles
    {
        public event Action TreeUpdate;
        public event Action CurrentFilenameChanged;
        public event Action EndOfSearch;
        string currentFilename;
        string currentFolder;
        int numOfFiles;
        DateTime now;
        DateTime start;

        string basefolder;
        string mask;
        string text;
        TreeNode tree;

        public string CurrentFilename { get => currentFilename; }
        public string CurrentFolder { get => currentFolder; }
        public int NumOfFiles { get => numOfFiles; }
        public DateTime Now { get => now; set => now = value; }
        public DateTime Start { get => start; set => start = value; }
        public TreeNode Tree { get => tree; }

        /// <summary>
        /// Представляет класс поиска.
        /// </summary>
        /// <param name="basefolder">Начальная папка.</param>
        /// <param name="mask">Маска файлов.</param>
        /// <param name="text">Искомый текст.</param>
        public FindFiles()
        {
        }

        /// <summary>
        /// Метод начала поиска в каталогах.
        /// </summary>
        /// <param name="basefolder"></param>
        /// <param name="mask"></param>
        /// <param name="text"></param>
        public void StartSearch(string basefolder, string mask, string text)
        {
            numOfFiles = 0;
            this.basefolder = basefolder;
            this.mask = mask;
            this.text = text;
            tree = new TreeNode();
            Search(basefolder, null);
            EndOfSearch?.Invoke();
        }

        /// <summary>
        /// Функция рекурсивного перебора папок и построение дерева TreeNode.
        /// </summary>
        /// <param name="basefolder">Текущая папка.</param>
        /// <param name="sub">Текущий TreeNode.</param>
        /// <param name="tree">Ссылка на дерево для построения.</param>
        /// <returns></returns>
        public bool Search(string basefolder, TreeNode sub)
        {
            if (sub == null)
                sub = new TreeNode(basefolder);
            int i = 0;
            DirectoryInfo di = new DirectoryInfo(basefolder);
            DirectoryInfo[] subfolders = di.GetDirectories();
            foreach (DirectoryInfo subfolder in subfolders)
            {
                sub.Nodes.Add(subfolder.FullName, subfolder.Name);
                if (Search(subfolder.FullName, sub.Nodes[i]))
                {
                    i++;
                    currentFolder = subfolder.Name;
                    currentFilename = string.Empty;
                    CurrentFilenameChanged?.Invoke();
                }
            }
            FileInfo[] files = di.GetFiles(mask);
            bool flag = false;
            foreach (FileInfo file in files)
            {
                currentFilename = file.Name;
                numOfFiles++;
                CurrentFilenameChanged?.Invoke();
                if (File.ReadAllText(file.FullName, GetEncoding(file.FullName)).Contains(text))
                {
                    flag = true;
                    sub.Nodes.Add(file.FullName, file.Name);
                    var tmp = sub;
                    while (tmp.Level >= 1)
                    {
                        tmp = tmp.Parent;
                    }
                    if (tree.Text == string.Empty)
                        tree = tmp;
                    else
                    {
                        tree.Nodes.Add(tmp.Nodes[0].Clone() as TreeNode);
                    }
                    TreeUpdate?.Invoke();
                }
            }
            if (!flag && sub != null && sub.Nodes.Count == 0)
            {
                sub.Remove();
                return false;
            }
            return true;
        }


        /// <summary>
        /// Метод определения кодировки файла.
        /// Решение взято со StackOverflow: https://stackoverflow.com/questions/3825390/effective-way-to-find-any-files-encoding
        /// Причина: не во всех файлах мог осуществляться поиск, самостоятельно обойти эту проблему не смог.
        /// </summary>
        /// <param name="filename">Имя файла.</param>
        /// <returns>Кодировка файла.</returns>
        public static Encoding GetEncoding(string filename)
        {
            // Read the BOM
            var bom = new byte[4];
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                file.Read(bom, 0, 4);
            }

            // Analyze the BOM
            if (bom[0] == 0x2b && bom[1] == 0x2f && bom[2] == 0x76) return Encoding.UTF7;
            if (bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf) return Encoding.UTF8;
            if (bom[0] == 0xff && bom[1] == 0xfe) return Encoding.Unicode; //UTF-16LE
            if (bom[0] == 0xfe && bom[1] == 0xff) return Encoding.BigEndianUnicode; //UTF-16BE
            if (bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff) return Encoding.UTF32;
            return Encoding.ASCII;
        }
    }
}