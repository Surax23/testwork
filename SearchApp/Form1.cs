﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SearchApp
{

    public partial class Form1 : Form
    {
        FindFiles findFiles;

        public Form1()
        {
            InitializeComponent();

            findFiles = new FindFiles();
            findFiles.TreeUpdate += ShowTree;
            findFiles.CurrentFilenameChanged += FileNameChanged;
            findFiles.EndOfSearch += TimerStop;

            tb_FolderName.Text = Properties.Settings.Default.FolderName;
            tb_Mask.Text = Properties.Settings.Default.Mask;
            tb_Text.Text = Properties.Settings.Default.Text;
        }

        /// <summary>
        /// Завершение рекурсивного алгоритма, сохранение текущего времени.
        /// </summary>
        private void TimerStop()
        {
            findFiles.Now = DateTime.Now;
        }

        /// <summary>
        /// Обработчик события изменения текущего проверяемого файла.
        /// </summary>
        private void FileNameChanged()
        {
            stt_Current.Text = $"Текущий файл: {findFiles.CurrentFolder}/{findFiles.CurrentFilename}";
            stt_Num.Text = $"Обработано: {findFiles.NumOfFiles}";
            findFiles.Now = DateTime.Now;
            TimeSpan diff = findFiles.Now - findFiles.Start;
            stt_Time.Text = $"Прошло времени: {diff.Minutes}:{diff.Seconds}";
            this.Update();
        }

        /// <summary>
        /// Обработчик события изменения дерева в классе, вывод на экран.
        /// </summary>
        private void ShowTree()
        {
            treeFiles.Nodes.Clear();
            treeFiles.Nodes.Add(findFiles.Tree);
            treeFiles.ExpandAll();
            this.Update();
        }

        /// <summary>
        /// Обработчик выбора каталога.
        /// </summary>
        private void ChooseCatalog_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
                tb_FolderName.Text = fbd.SelectedPath;
        }

        /// <summary>
        /// Обработчик нажтия кнопки "Play".
        /// </summary>
        private void Start_Click(object sender, EventArgs e)
        {
            treeFiles.Nodes.Clear();
            findFiles.Start = DateTime.Now;
            findFiles.StartSearch(tb_FolderName.Text, tb_Mask.Text, tb_Text.Text);
        }

        /// <summary>
        /// Обработка двойного щелчка по узлу в treeView.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Nodes.Count == 0)
            {
                System.Diagnostics.Process.Start("explorer", e.Node.FullPath);
            }
        }

        /// <summary>
        /// Обработчик закрытия формы.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.FolderName = tb_FolderName.Text;
            Properties.Settings.Default.Mask = tb_Mask.Text;
            Properties.Settings.Default.Text = tb_Text.Text;
            Properties.Settings.Default.Save();
        }
    }
}