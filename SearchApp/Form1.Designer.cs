﻿namespace SearchApp
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_FolderName = new System.Windows.Forms.TextBox();
            this.btn_ChooseCatalog = new System.Windows.Forms.Button();
            this.btn_Start = new System.Windows.Forms.Button();
            this.tb_Mask = new System.Windows.Forms.TextBox();
            this.tb_Text = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.stt_Num = new System.Windows.Forms.ToolStripStatusLabel();
            this.stt_Time = new System.Windows.Forms.ToolStripStatusLabel();
            this.stt_Current = new System.Windows.Forms.ToolStripStatusLabel();
            this.treeFiles = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_FolderName
            // 
            this.tb_FolderName.Location = new System.Drawing.Point(13, 27);
            this.tb_FolderName.Name = "tb_FolderName";
            this.tb_FolderName.ReadOnly = true;
            this.tb_FolderName.Size = new System.Drawing.Size(193, 20);
            this.tb_FolderName.TabIndex = 0;
            // 
            // btn_ChooseCatalog
            // 
            this.btn_ChooseCatalog.Location = new System.Drawing.Point(212, 25);
            this.btn_ChooseCatalog.Name = "btn_ChooseCatalog";
            this.btn_ChooseCatalog.Size = new System.Drawing.Size(75, 23);
            this.btn_ChooseCatalog.TabIndex = 1;
            this.btn_ChooseCatalog.Text = "Browse...";
            this.btn_ChooseCatalog.UseVisualStyleBackColor = true;
            this.btn_ChooseCatalog.Click += new System.EventHandler(this.ChooseCatalog_Click);
            // 
            // btn_Start
            // 
            this.btn_Start.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_Start.Location = new System.Drawing.Point(212, 68);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(75, 23);
            this.btn_Start.TabIndex = 2;
            this.btn_Start.Text = "►";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // tb_Mask
            // 
            this.tb_Mask.Location = new System.Drawing.Point(13, 70);
            this.tb_Mask.Name = "tb_Mask";
            this.tb_Mask.Size = new System.Drawing.Size(193, 20);
            this.tb_Mask.TabIndex = 4;
            this.tb_Mask.Text = "*.*";
            // 
            // tb_Text
            // 
            this.tb_Text.Location = new System.Drawing.Point(16, 113);
            this.tb_Text.Multiline = true;
            this.tb_Text.Name = "tb_Text";
            this.tb_Text.Size = new System.Drawing.Size(193, 176);
            this.tb_Text.TabIndex = 5;
            this.tb_Text.Text = "cont";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stt_Num,
            this.stt_Time,
            this.stt_Current});
            this.statusStrip1.Location = new System.Drawing.Point(0, 606);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(822, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // stt_Num
            // 
            this.stt_Num.Name = "stt_Num";
            this.stt_Num.Size = new System.Drawing.Size(78, 17);
            this.stt_Num.Text = "Обработано:";
            // 
            // stt_Time
            // 
            this.stt_Time.Name = "stt_Time";
            this.stt_Time.Size = new System.Drawing.Size(109, 17);
            this.stt_Time.Text = "Прошло времени:";
            // 
            // stt_Current
            // 
            this.stt_Current.Name = "stt_Current";
            this.stt_Current.Size = new System.Drawing.Size(92, 17);
            this.stt_Current.Text = "Текущий файл:";
            // 
            // treeFiles
            // 
            this.treeFiles.Location = new System.Drawing.Point(294, 27);
            this.treeFiles.Name = "treeFiles";
            this.treeFiles.Size = new System.Drawing.Size(513, 262);
            this.treeFiles.TabIndex = 7;
            this.treeFiles.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.NodeMouseDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Директория для поиска:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Маска имени файла:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Текст в файле:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(294, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Результаты поиска:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(212, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Старт:";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(197, 309);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(505, 276);
            this.treeView1.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 628);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.treeFiles);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tb_Text);
            this.Controls.Add(this.tb_Mask);
            this.Controls.Add(this.btn_Start);
            this.Controls.Add(this.btn_ChooseCatalog);
            this.Controls.Add(this.tb_FolderName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "SearchFile";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_FolderName;
        private System.Windows.Forms.Button btn_ChooseCatalog;
        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.TextBox tb_Mask;
        private System.Windows.Forms.TextBox tb_Text;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel stt_Current;
        private System.Windows.Forms.TreeView treeFiles;
        private System.Windows.Forms.ToolStripStatusLabel stt_Num;
        private System.Windows.Forms.ToolStripStatusLabel stt_Time;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TreeView treeView1;
    }
}

